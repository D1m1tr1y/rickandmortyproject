package com.example.rickandmorty.root

import android.os.Bundle
import android.text.TextUtils.replace
import com.example.rickandmorty.R
import com.example.rickandmorty.common.BaseActivity
import com.example.rickandmorty.main_page.ui.MainPageFragment

class RootActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        replace(MainPageFragment(),R.id.container)
    }
}
