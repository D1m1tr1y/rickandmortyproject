package com.example.rickandmorty.main_page.db.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.rickandmorty.main_page.db.model.CharacterDataRoom
import com.example.rickandmorty.main_page.db.dao.CharacterDataRoomDao

@Database(
    entities = [CharacterDataRoom::class],
    version = 1
)
abstract class CharacterDataBase : RoomDatabase() {
    abstract val dao : CharacterDataRoomDao
}
