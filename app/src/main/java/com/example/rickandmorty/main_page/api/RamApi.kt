package com.example.rickandmorty.main_page.api

import com.example.rickandmorty.main_page.api.model.CharacterDataResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface RamApi {
    @GET("character")
    suspend fun getData(
        @Query("page")page : Int
    ):CharacterDataResponse
}