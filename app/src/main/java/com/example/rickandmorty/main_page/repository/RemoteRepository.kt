package com.example.rickandmorty.main_page.repository

import com.example.rickandmorty.main_page.model.CharacterData

interface RemoteRepository {
    suspend fun getData(page: Int): CharacterData
}
