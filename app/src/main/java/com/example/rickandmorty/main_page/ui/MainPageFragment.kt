package com.example.rickandmorty.main_page.ui

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.rickandmorty.R
import com.example.rickandmorty.common.BaseFragment
import com.example.rickandmorty.databinding.FragmentMainPageBinding
import com.example.rickandmorty.main_page.ui.recycler_view_adapter.MainPageRecyclerViewAdapter
import com.example.rickandmorty.utils.viewbinding.viewBinding
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainPageFragment : BaseFragment(R.layout.fragment_main_page) {
    private val viewModel: MainPageViewModel by viewModel()
    private val binding: FragmentMainPageBinding by viewBinding()
    private val adapter: MainPageRecyclerViewAdapter by lazy {
        MainPageRecyclerViewAdapter()
    }
    private val layoutManager: LinearLayoutManager by lazy {
        LinearLayoutManager(context)
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(binding) {
            recyclerView.layoutManager = layoutManager
            recyclerView.setHasFixedSize(true)
            recyclerView.adapter = adapter
            adapter.onAttachedToRecyclerView(recyclerView)
        }
        viewModel.setData()
        viewModel.loadData(1)
        observe(viewModel.characterData) {
            adapter.setData(it)
        }
    }

}
