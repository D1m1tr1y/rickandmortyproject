package com.example.rickandmorty.main_page.ui

import com.example.rickandmorty.common.BaseViewModel
import com.example.rickandmorty.main_page.interactor.MainPageInteractor
import com.example.rickandmorty.main_page.ui.model.ResultsUi
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import timber.log.Timber

class MainPageViewModel(
    private val mainPageInteractor: MainPageInteractor
) : BaseViewModel() {
    private val _characterData = MutableStateFlow<List<ResultsUi>>(emptyList())
    val characterData = _characterData.asStateFlow()

    fun setData() {
        launch {
            _characterData.value = mainPageInteractor.getCharacters()
        }
    }

    fun loadData(page: Int) =
        launch {
            try {
                mainPageInteractor.upsertCharacters(page)

            } catch (e: CancellationException) {
                Timber.e(e.message)
            } catch (t: Throwable) {
                Timber.e(t.message)
            }
        }
}