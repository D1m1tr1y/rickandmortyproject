package com.example.rickandmorty.main_page.repository

import com.example.rickandmorty.main_page.db.dao.CharacterDataRoomDao
import com.example.rickandmorty.main_page.db.model.RoomDataConverter
import com.example.rickandmorty.main_page.ui.model.ResultsUi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class MainPageLocalRepository(
    private val dao: CharacterDataRoomDao
) : LocalRepository {
    override suspend fun upsertCharacters(data: List<ResultsUi>) {
        val response = RoomDataConverter.toDataBase(data)
        dao.upsertCharacter(response)
    }

    override suspend fun getCharacters(): Flow<List<ResultsUi>> {
        return dao.getCharacters().map { list ->
            list.map { RoomDataConverter.fromDatabase(it) }
        }
    }
}