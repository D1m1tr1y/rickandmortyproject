package com.example.rickandmorty.main_page.ui.recycler_view_adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.rickandmorty.R
import com.example.rickandmorty.main_page.ui.model.ResultsUi
import timber.log.Timber

class MainPageRecyclerViewAdapter : RecyclerView.Adapter<MainPageRecyclerViewViewHolder>() {
    private val charactersData = mutableListOf<ResultsUi>()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainPageRecyclerViewViewHolder {
        LayoutInflater.from(parent.context).inflate(R.layout.item_recycler_view, parent, false)
        return MainPageRecyclerViewViewHolder(parent)
    }

    override fun getItemCount() = charactersData.size


    override fun onBindViewHolder(holder: MainPageRecyclerViewViewHolder, position: Int) {
        val listItem = charactersData[position]

        holder.onBind(listItem)
    }
    @SuppressLint("NotifyDataSetChanged")
    fun setData( values : List<ResultsUi>) {
        Timber.i("_______$values")
        charactersData.clear()
        charactersData.addAll(values)
        notifyDataSetChanged()
    }
}