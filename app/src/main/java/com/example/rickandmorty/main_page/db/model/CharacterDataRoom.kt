package com.example.rickandmorty.main_page.db.model

import androidx.room.Entity
import androidx.room.PrimaryKey
@Entity
data class CharacterDataRoom(
    @PrimaryKey
    val name: String,
    val status: String,
    val species: String,
    val gender: String,
    val origin: String,
    val location: String,
    val image: String,
)
