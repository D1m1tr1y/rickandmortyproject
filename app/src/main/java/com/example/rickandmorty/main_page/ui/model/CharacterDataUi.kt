package com.example.rickandmorty.main_page.ui.model

data class CharacterDataUi(
    val result : List<ResultsUi>
)
