package com.example.rickandmorty.main_page.model

import com.google.gson.annotations.SerializedName

data class CharacterData(
    @SerializedName("info")
    val info : Info,
    @SerializedName("results")
    val results : List<Results>
)
