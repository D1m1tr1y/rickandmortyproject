package com.example.rickandmorty.main_page.interactor

import com.example.rickandmorty.main_page.repository.LocalRepository
import com.example.rickandmorty.main_page.repository.RemoteRepository
import com.example.rickandmorty.main_page.ui.model.CharacterDataConverterUi
import com.example.rickandmorty.main_page.ui.model.ResultsUi

class MainPageInteractor(
    private val mainPageRemoteRepository: RemoteRepository,
    private val mainPageLocalRepository: LocalRepository
) {
    suspend fun getCharacters(): List<ResultsUi> {
        var characterData = emptyList<ResultsUi>()
       mainPageLocalRepository.getCharacters().collect{
          characterData = it
      }
        return  characterData
    }
    suspend fun upsertCharacters(page : Int){
        val data = CharacterDataConverterUi.fromNetwork(mainPageRemoteRepository.getData(page))
        mainPageLocalRepository.upsertCharacters(data.result)
    }

}