package com.example.rickandmorty.main_page.repository

import com.example.rickandmorty.main_page.ui.model.CharacterDataUi
import com.example.rickandmorty.main_page.ui.model.ResultsUi
import kotlinx.coroutines.flow.Flow

interface LocalRepository {
   suspend fun upsertCharacters(data: List<ResultsUi>)
  suspend  fun getCharacters  (): Flow<List<ResultsUi>>
}