package com.example.rickandmorty.main_page.repository

import com.example.rickandmorty.main_page.api.RamApi
import com.example.rickandmorty.main_page.model.CharacterData
import com.example.rickandmorty.main_page.model.CharacterDataConverter

class MainPageRemoteRepository(
    private val api: RamApi
): RemoteRepository{
    override suspend fun getData(
        page: Int
    ): CharacterData =
      CharacterDataConverter.fromNetwork(api.getData(page))
}
