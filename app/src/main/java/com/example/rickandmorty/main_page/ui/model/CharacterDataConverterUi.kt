package com.example.rickandmorty.main_page.ui.model

import com.example.rickandmorty.main_page.api.model.CharacterDataResponse
import com.example.rickandmorty.main_page.api.model.ResultsResponse
import com.example.rickandmorty.main_page.model.CharacterData
import com.example.rickandmorty.main_page.model.Results

object CharacterDataConverterUi {
    fun fromNetwork (response : CharacterData) : CharacterDataUi =
        CharacterDataUi(
            result = resultsConverter(response.results)
        )
    private fun resultsConverter(response : List<Results>) =
        response.map{result ->
            ResultsUi(
                name = result.name,
                status = result.status,
                species = result.species,
                gender = result.gender,
                origin = result.origin.name,
                location = result.location.name,
                image = result.image
            )
        }

}