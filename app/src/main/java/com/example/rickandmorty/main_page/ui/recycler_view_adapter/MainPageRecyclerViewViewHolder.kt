package com.example.rickandmorty.main_page.ui.recycler_view_adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.rickandmorty.databinding.ItemRecyclerViewBinding
import com.example.rickandmorty.main_page.ui.model.ResultsUi

class MainPageRecyclerViewViewHolder(
    private val  binding:ItemRecyclerViewBinding
): RecyclerView.ViewHolder(binding.root)
{
    constructor(
        parent: ViewGroup
    ): this(
        ItemRecyclerViewBinding.inflate(LayoutInflater.from(parent.context), parent, false)
    )
    fun onBind(character: ResultsUi ){
        with(binding){
            characterName.text = character.name
            image.load(character.image)
        }

    }

}