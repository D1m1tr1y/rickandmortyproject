package com.example.rickandmorty.main_page.db.model

import com.example.rickandmorty.main_page.ui.model.ResultsUi

object RoomDataConverter {

    fun toDataBase(response: List<ResultsUi>) =
        response.map { result ->
            CharacterDataRoom(
                name = result.name,
                status = result.status,
                species = result.species,
                gender = result.gender,
                origin = result.name,
                location = result.location,
                image = result.image
            )
        }
    fun fromDatabase(result : CharacterDataRoom) =
            ResultsUi(
                name = result.name,
                status = result.status,
                species = result.species,
                gender = result.gender,
                origin = result.name,
                location = result.location,
                image = result.image
            )
}