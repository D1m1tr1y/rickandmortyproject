package com.example.rickandmorty.main_page.di

import androidx.room.Room
import com.example.rickandmorty.common.di.InjectionModule
import com.example.rickandmorty.main_page.api.RamApi
import com.example.rickandmorty.main_page.interactor.MainPageInteractor
import com.example.rickandmorty.main_page.repository.LocalRepository
import com.example.rickandmorty.main_page.repository.MainPageLocalRepository
import com.example.rickandmorty.main_page.repository.MainPageRemoteRepository
import com.example.rickandmorty.main_page.repository.RemoteRepository
import com.example.rickandmorty.main_page.db.database.CharacterDataBase
import com.example.rickandmorty.main_page.ui.MainPageViewModel
import org.koin.androidx.viewmodel.dsl.viewModelOf
import org.koin.core.module.dsl.factoryOf
import org.koin.core.module.dsl.singleOf
import org.koin.dsl.bind
import org.koin.dsl.module
import retrofit2.Retrofit

object MainPageModule : InjectionModule {
    override fun create() =
        module {
            single{get<Retrofit>().create(RamApi::class.java)}
            single{
                Room.databaseBuilder(get(), CharacterDataBase::class.java, "Rick and Morty")
                    .build()
            }
            single { get<CharacterDataBase>().dao }
            single <LocalRepository>{MainPageLocalRepository(get())}
            singleOf(::MainPageLocalRepository) bind LocalRepository::class
            single<RemoteRepository>{ MainPageRemoteRepository(get()) }
            singleOf(::MainPageRemoteRepository) bind RemoteRepository::class
            factoryOf(::MainPageInteractor)
            viewModelOf(::MainPageViewModel)
        }
}
