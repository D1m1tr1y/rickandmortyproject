package com.example.rickandmorty.main_page.db.dao

import androidx.room.Dao
import androidx.room.Query
import androidx.room.Upsert
import com.example.rickandmorty.main_page.db.model.CharacterDataRoom
import kotlinx.coroutines.flow.Flow
@Dao
interface CharacterDataRoomDao {
    @Upsert
   suspend fun upsertCharacter( characterData : List<CharacterDataRoom>)
   @Query("SELECT * FROM CharacterDataRoom ")
   fun getCharacters() : Flow<List<CharacterDataRoom>>
}
