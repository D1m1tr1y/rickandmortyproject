package com.example.rickandmorty.main_page.api.model

import com.google.gson.annotations.SerializedName

data class CharacterDataResponse(
    @SerializedName("info")
    val info : InfoResponce,
    @SerializedName("results")
    val results : List<ResultsResponse>
)
