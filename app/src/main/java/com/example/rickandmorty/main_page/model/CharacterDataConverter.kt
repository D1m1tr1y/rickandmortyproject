package com.example.rickandmorty.main_page.model

import com.example.rickandmorty.main_page.api.model.CharacterDataResponse
import com.example.rickandmorty.main_page.api.model.InfoResponce
import com.example.rickandmorty.main_page.api.model.LocationResponse
import com.example.rickandmorty.main_page.api.model.OriginResponse
import com.example.rickandmorty.main_page.api.model.ResultsResponse

object CharacterDataConverter {
    fun fromNetwork(dataResponse: CharacterDataResponse): CharacterData =
        CharacterData(
            info = fromNetwork(response = dataResponse.info),
            results = resultsConverter(dataResponse.results)
        )

    private fun resultsConverter(response: List<ResultsResponse>) =
        response.map { result ->
            Results(
                name = result.name,
                status = result.status,
                species = result.species,
                gender = result.gender,
                origin = fromNetwork(result.origin),
                location = fromNetwork(result.location),
                image = result.image,
                id = result.id,
                type = result.type,
                episode = result.episode,
                url = result.url,
                created = result.created
            )
        }

    private fun fromNetwork(response: InfoResponce) =
        Info(
            count = response.count,
            next = response.next,
            pages = response.pages,
            prev = response.prev
        )

    private fun fromNetwork(response: OriginResponse) =
        Origin(
            name = response.name,
            url = response.url
        )

    private fun fromNetwork(response: LocationResponse) =
        Location(
            name = response.name,
            url = response.url
        )
}
