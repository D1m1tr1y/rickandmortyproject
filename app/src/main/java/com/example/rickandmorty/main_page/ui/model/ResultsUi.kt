package com.example.rickandmorty.main_page.ui.model

data class ResultsUi(
    val name: String,
    val status: String,
    val species: String,
    val gender: String,
    val origin: String,
    val location: String,
    val image: String,
)
