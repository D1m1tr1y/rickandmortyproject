package com.example.rickandmorty.utils

import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch



fun Fragment.replaceFragment(fragment: Fragment, id: Int) {
    val fragmentManager = activity?.supportFragmentManager
    fragmentManager?.
    beginTransaction()?.
    addToBackStack(null)?.
    replace(id, fragment)?.
    commit()
}
